<?php
/**
 * Created by PhpStorm.
 * User: lbob
 * Date: 2014/11/14
 * Time: 11:49
 */

return [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'xaircraft',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix'    => 'x_'
];